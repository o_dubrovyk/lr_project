# README #

How to run this project:

1. Clone repositiry: git clone https://o_dubrovyk@bitbucket.org/o_dubrovyk/lr_project.git
2. Create venv inside project dir: python3 -m venv .
3. Activate venv source venv/bin/activete
4. Install requirements: pip install -e . or pip install setup.py
5. Set ENV variables: export FLASK_ENV=development, export FLASK_APP=app
6. Run project: flask run
7. Go to localhost:5000/api/v1.0/upload