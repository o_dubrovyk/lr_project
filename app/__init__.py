from flask import Flask, make_response, render_template, request
from .additional_functions import to_pretty_json
import werkzeug
from .config import Config


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(Config)
    from . import views
    app.register_blueprint(views.bp)
    app.jinja_env.filters['tojson_pretty'] = to_pretty_json

    @app.errorhandler(werkzeug.exceptions.NotFound)
    def not_found(e):
        return make_response(render_template('404.html', request_url=request.url), 404)

    @app.errorhandler(werkzeug.exceptions.BadRequest)
    def handle_bad_request(e):
        return make_response(render_template('400.html', request_url=request.url), 400)

    return app
