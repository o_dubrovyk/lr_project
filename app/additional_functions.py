import os
import warnings
import json
import shutil
import matplotlib

import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
import seaborn as sns

from multiprocessing import Process
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score

matplotlib.use("Agg")
warnings.filterwarnings('ignore')

ALLOWED_EXTENSIONS = {'csv'}


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def read_csv(filename):
    data_set = pd.DataFrame(pd.read_csv(filename))
    return data_set


def dict_to_pd_df(dict_obj):
    return pd.DataFrame(dict_obj)


def background_remove(path):
    task = Process(target=rm(path))
    task.start()


def rm(path):
    os.remove(path)


def clean_folder(path):
    for file in os.scandir(path):
        os.remove(file.path)


def to_pretty_json(value):
    return json.dumps(value, sort_keys=True,
                      indent=4, separators=(',', ': '))


def compile_report(data_set):
    data = {}
    data["desc_stat"] = data_set.describe().to_dict()
    data_set.isnull().sum() * 100 / data_set.shape[0]
    fig, axs = plt.subplots(3, figsize=(5, 5))
    plt1 = sns.boxplot(data_set['TV'], ax=axs[0])
    plt2 = sns.boxplot(data_set['Newspaper'], ax=axs[1])
    plt3 = sns.boxplot(data_set['Radio'], ax=axs[2])
    plt.tight_layout()
    plt.savefig(os.getcwd() + "/app/plots/x_vars_box_plot.png")
    plt.clf()
    sns.boxplot(data_set['Sales'])
    plt.savefig(os.getcwd() + "/app/plots/y_var_box_plot.png")
    plt.clf()
    sns.pairplot(data_set, x_vars=['TV', 'Newspaper', 'Radio'], y_vars='Sales', height=4, aspect=1, kind='scatter')
    plt.savefig(os.getcwd() + "/app/plots/relationships.png")
    plt.clf()
    sns.heatmap(data_set.corr(), cmap="YlGnBu", annot=True)
    plt.savefig(os.getcwd() + "/app/plots/heat_map.png")
    plt.clf()
    X = data_set['TV']
    y = data_set['Sales']
    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.7, test_size=0.3, random_state=100)
    data["x_train_head"] = X_train.head().to_dict()
    data["y_train_head"] = y_train.head().to_dict()
    X_train_sm = sm.add_constant(X_train)
    lr = sm.OLS(y_train, X_train_sm).fit()
    results_summary = lr.summary2()
    data["lr_summary"] = {}
    i = 0
    for table in results_summary.tables:
        data["lr_summary"]["table_"+str(i)] = table.to_dict(orient='index')
        i = i+1
    plt.scatter(X_train, y_train)
    plt.plot(X_train, lr.params[0] + lr.params[1] * X_train, 'r')
    plt.savefig(os.getcwd() + "/app/plots/lr_train_plot.png")
    plt.clf()
    y_train_pred = lr.predict(X_train_sm)
    res = (y_train - y_train_pred)
    fig = plt.figure()
    sns.distplot(res, bins=15)
    fig.suptitle('Error Terms', fontsize=15)
    plt.xlabel('y_train - y_train_pred', fontsize=15)
    plt.savefig(os.getcwd() + "/app/plots/error_terms.png")
    plt.clf()
    plt.scatter(X_train, res)
    plt.savefig(os.getcwd() + "/app/plots/residuals.png")
    plt.clf()
    X_test_sm = sm.add_constant(X_test)
    y_pred = lr.predict(X_test_sm)
    data["test_set_stat"] = {}
    data["test_set_stat"]["test_MSE"] = np.sqrt(mean_squared_error(y_test, y_pred))
    data["test_set_stat"]["test_r2"] = r2_score(y_test, y_pred)
    plt.scatter(X_test, y_test)
    plt.plot(X_test, lr.params[0] + lr.params[1] * X_test, 'r')
    plt.savefig(os.getcwd() + "/app/plots/lr_test_plot.png")
    plt.clf()
    shutil.make_archive("app/comp_plots", 'tar', os.getcwd() + "/app/plots")
    clean_folder(os.getcwd() + "/app/plots/")
    return data
