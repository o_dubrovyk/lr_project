import os


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or os.urandom(16)
    UPLOAD_FOLDER = os.environ.get('UPLOAD_FOLDER') or f'{os.getcwd()}/app/uploads'
    DEBUG = os.environ.get('DEBUG') or True
    CLIENT_IMAGES = os.environ.get('CLIENT_IMAGES') or f'{os.getcwd()}/app'

