import os
import app.additional_functions as ad_funcs
from flask import request, redirect, flash, Blueprint, current_app, render_template, session, abort, send_from_directory
from werkzeug.utils import secure_filename


bp = Blueprint('views', __name__, url_prefix='/api/v1.0')

@bp.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        file = request.files['file']
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        elif file and ad_funcs.allowed_file(filename=file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
            data_set = ad_funcs.read_csv(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
            session['data_set'] = data_set.to_dict('list')
            ad_funcs.background_remove(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
            result = ad_funcs.compile_report(data_set)
            return render_template("compute.html", response=result, title="Compute")
    return render_template('upload.html', title="Upload")


@bp.route('/download', methods=['GET'])
def download():
    try:
        return send_from_directory(current_app.config['CLIENT_IMAGES'], 'comp_plots.tar', as_attachment=True)
    except FileNotFoundError:
        abort(404)




