from setuptools import find_packages, setup

setup(
    name='slr_project',
    version='1.0.1',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
        'numpy',
        'pandas',
        'matplotlib',
        'seaborn',
        'statsmodels',
        'sklearn',
        'lxml'
    ],
)